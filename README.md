 * Repositorio Name: Snippets WordPress
 * Repositorio URL: https://gitlab.com/-/ide/project/Chabi112/Snippets-WordPress
 * Descripción: Todos los snippets que utilizo en WordPress y que quiero compartir contigo. Los he utilizado, lo mismo en proyectos de clientes como mios. 
 * Version: 1.3
 * Author: Chabi Angulo
 * Author URI: https://paginaswebzaragoza.com

# Snippets WordPress
Los snippets que voy añadiendo en este repositorio los suelo utilizar en muchos proyectos de clientes y en mis propios proyectos.
Son para WordPress, el CMS que muchos conoceis.

## Changelog

- v1.0.0  06/10/2022
Añadido Snippet - Nombrar Mi Cuenta WooCommerce ||| Fuente: Fran Salas - PluginsWeb

- v1.1 07/10/2022
Añadido Snippet - Definir el número de revisiones ||| Fuente: Fernando Tellado - Libro WordPress 1001 trucos

- v1.2 27/10/2022
Añadido Snippet - Nuevo tamaño de imagen ||| Fuente Desconocido

- v1.3 14/12/2022
Añadido Snippet - Cómo quitar el scroll o desplazamiento horizontal ||| Fuente: Fernando Tellado - Ayuda WordPress
## Fuentes

Aquí pondré las fuentes de donde he extraído los Snippets dando las gracias a todos ellos.

- PluginsWeb de Fran Sala - [Canal de YouTube](https://www.youtube.com/channel/UCq5YfQoG0Sg1zJ06sUSn2Cw)
- [Libro "WordPress 1001 trucos"](https://www.amazon.es/WordPress-1001-trucos-T%C3%ADtulos-Especiales/dp/8441538255/ref=sr_1_1?__mk_es_ES=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=2KTSJG54JKZGX&keywords=1001+trucos&qid=1666903521&sprefix=1001+trucos%2Caps%2C75&sr=8-1) de Fernando Tellado
- Ayuda WordPress de Fernando Tellado - https://ayudawp.com
