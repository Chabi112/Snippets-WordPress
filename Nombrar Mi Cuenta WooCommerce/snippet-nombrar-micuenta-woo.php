/* Nombrar Mi Cuenta en WooCommerce para los que no está logueados o si*/
function pw_dynamic_menu_item_label( $items, $args ) {
   if ( ! is_user_logged_in() ) {
      $items = str_replace( "Mi cuenta", "Acceso/Registro", $items );
   }
   return $items;
}

add_filter( 'wp_nav_menu_items', 'pw_dynamic_menu_item_label', 9999, 2 );
